
# SETUP ENV
service network-manager stop
ip addr flush dev eth0
ip addr flush dev eth1

brctl addbr TR_BR
ip link set TR_BR up

ip link add MAN_1 type veth peer name MAN_2
ip link set MAN_1 up
ip link set MAN_2 up

brctl addif TR_BR eth1
brctl addif TR_BR MAN_1

dhclient TR_BR

ip link add ve1.1 type veth peer name ve1.2
ip link set ve1.2 up

ip link add ve2.1 type veth peer name ve2.2
ip link set ve2.2 up

brctl addbr EXIT_BR
ip link set EXIT_BR up
brctl addif EXIT_BR ve1.2
brctl addif EXIT_BR ve2.2
ip addr add 10.56.0.254/24 dev EXIT_BR

ip netns add natr1
ip netns exec natr1 ip link set lo up
ip netns add natr2
ip netns exec natr2 ip link set lo up

ip link set ve1.1 netns natr1
ip netns exec natr1 ip link set ve1.1 up

ip link set ve2.1 netns natr2
ip netns exec natr2 ip link set ve2.1 up

ip netns exec natr1 ip addr add 10.56.0.201/24 dev ve1.1
ip netns exec natr2 ip addr add 10.56.0.201/24 dev ve1.1

ip netns exec natr1 ip route add default via 10.56.0.254
ip netns exec natr2 ip route add default via 10.56.0.254

iptables -t nat -A POSTROUTING -o TR_BR -j MASQUERADE
iptables -A FORWARD -i EXIT_BR -j ACCEPT

echo 1 >/proc/sys/net/ipv4/ip_forward




# SETUP NEW EXIT ZONE
ip netns add natr1
ip netns exec natr1 ip link set lo up

ip link add ve1.1 type veth peer name ve1.2
ip link set ve1.1 netns natr1
ip netns exec natr1 ip link set ve1.1 up
brctl addif EXIT_BR ve1.2
ip link set ve1.2 up
ip netns exec natr1 ip addr add 10.56.0.201/24 dev ve1.1
ip netns exec natr1 ip route add default via 10.56.0.254


