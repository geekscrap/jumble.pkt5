''' Zone meta '''
long_name		= 'NAT exit zone'
category		= 'NAT;EXIT'
author			= 'Matthew Holley'
version			= 1.0
description		= 'This zone forwards traffic to the real network gateway'

''' Zone attributes '''
name			= 'exit_nat'
gates			= (1,0)			# ONLY EVER REQUIRES ONE
buffer			= True
args			= None