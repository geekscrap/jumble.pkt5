from lib.parent_zone import parent_zone
from lib.sr import sr
from lib.const import EXIT_BR, UNTRUST_GW_IP, UNTRUST_NW, UNTRUST_NMASK, CLIENT_LIMIT
from lib import cmd_issue
import avail_zones.exit_nat.config as zconf

import logging, logging.handlers, commands, subprocess, binascii, socket
from multiprocessing import Process, Queue, Manager

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)

		self.pkg = {}
		self.threads = []

		self.nat_table = Manager().dict()

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		logger.info('Setting up exit zones')
		''' Create a namespace that has a pair of veths that joins to the EXIT_BR (for NAT) '''
		self.join2exit()

		nat_mac_ip = {}
		nat_mac_ip[UNTRUST_GW_IP] = commands.getoutput('ifconfig %s | grep HWaddr | awk \'{print $5}\'' % (EXIT_BR))

		''' Create the snd and rcv queues prior to dropping to nsname so we can use them later '''
		snd_q = Queue()
		rcv_q = Queue()

		''' Startup the translation processes '''
		pkt_out_th = Process(target=self.pkt_out, args=(self.nat_table, snd_q, zone_gates['left_0']))
		pkt_out_th.start()

		sr2nsname_th = Process(target=self.sr2nsname, args=(nat_mac_ip, snd_q, rcv_q))
		sr2nsname_th.start()

	def sr2nsname(self, nat_mac_ip, snd_q, rcv_q):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.sr2nsname')

		self.setns(self.pkg['nsname'])
		_sr = sr(iface=self.pkg['ve_1'], staticmac=nat_mac_ip, snd_q=snd_q, rcv_q=rcv_q)

		_sr.start()

		logger.debug('sr (socket send and receive) started: namespace=%s, iface=%s' % (self.pkg['nsname'], self.pkg['ve_1']))

	def pkt_out(self, nat_table, snd_q, zone_gate_q):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.pkt_out')

		rcv, snd = zone_gate_q

		''' Start at 2 as the upper limit is allocated to the gateway '''
		allocIP = 2

		for xIP in iter(rcv.get, None):

			bIP = bytearray(xIP)

			logger.debug('Got packet: %s' % (str(bIP).encode('hex')))

			xSrcIP = bIP[12:16]

			''' Test to see if the source IP already has an allocation '''
			if str(xSrcIP) in nat_table:
				logger.debug('Found src IP in NAT table: %s' % (str(xSrcIP).encode('hex')))
				x, xNewIP = nat_table[str(xSrcIP)]
			else:
				''' Test to see if the allocation of IP addresses has reached the limit '''
				if allocIP >= CLIENT_LIMIT:
					logger.error('Client limit reached: LIMIT=%s, dropping packet' % (CLIENT_LIMIT))
					continue

				''' Calculte the new IP '''
				transIP = str( UNTRUST_NW[:(UNTRUST_NW.rfind('.') + 1)] ) + str( int(UNTRUST_GW_IP[(UNTRUST_GW_IP.rfind('.') + 1):]) - allocIP )
				xNewIP = binascii.hexlify(socket.inet_aton(transIP)).decode('hex')
				''' Add a the new IP to the table '''
				nat_table[str(xSrcIP)] = (allocIP, xNewIP)

				logger.debug('New client added to NAT table: New IP[%s], translated IP [%s]' % (str(xSrcIP).encode('hex'), str(xNewIP).encode('hex')))

				allocIP += 1

			bIP[12:16] = xNewIP

			logger.debug('Recalculating IP checksum')
			''' Recalculate checksum '''

			logger.debug('Passing packet to sr')
			snd_q.put(str(bIP))


			'''
			xIP = sr.rcv_q.get()
			bIP = bytearray(xIP)

			bIP[20] = '\x00'

			srcip = bIP[12:16]
			dstip = bIP[16:20]

			bIP[12:16] = dstip
			bIP[16:20] = srcip
			'''
			'''
			extract the src ip
			save to nat_table[srcip] = pkg['ip'] - 1

			exchange old ip for new ip
			recalculate the IP header chksum

			send down to sr_rcv_q
			'''

	def pkt_in(self):
		pass

	def join2exit(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.join2exit')

		''' Create a package that describes the new zone that has been created '''
		pkg = {}

		pkg['nsname']	= '%s-%s' % (self.uid, self.name)
		pkg['ip']		= '%s%s' % ( str( UNTRUST_NW[:(UNTRUST_NW.rfind('.') + 1)] ), str( int(UNTRUST_GW_IP[(UNTRUST_GW_IP.rfind('.') + 1):]) - 1 ) )
		pkg['ve_1']		= '%s-ve1' % (self.uid)
		pkg['ve_2']		= '%s-ve2' % (self.uid)

		logger.info( 'Setting up exit namespace: %s' % (pkg['nsname']) )

		cmd = []
		''' Construct the command list to create the exit zone '''
		cmd.append((	('ip netns add %s' % (pkg['nsname'])),																	('ip netns del %s' % (pkg['nsname'])) ))
		cmd.append((	('ip netns exec %s ip link set lo up' % (pkg['nsname'])),												(None) ))
		cmd.append((	('ip link add %s type veth peer name %s' % (pkg['ve_1'], pkg['ve_2'])),									('ip link del %s' % pkg['ve_2']) ))
		cmd.append((	('ip link set %s netns %s' % (pkg['ve_1'], pkg['nsname'])),												(None) ))
		cmd.append((	('ip netns exec %s ip link set %s up' % (pkg['nsname'], pkg['ve_1'])),									(None) ))
		cmd.append((	('brctl addif %s %s' % (EXIT_BR, pkg['ve_2'])),															('brctl delif %s %s' % (EXIT_BR, pkg['ve_2'])) ))
		cmd.append((	('ip link set %s up' % (pkg['ve_2'])),																	(None) ))
		cmd.append((	('ip netns exec %s ip addr add %s%s dev %s' % (pkg['nsname'], pkg['ip'], UNTRUST_NMASK, pkg['ve_1'])),	(None) ))
		cmd.append((	('ip netns exec %s ip route add default via %s' % (pkg['nsname'], UNTRUST_GW_IP)),						(None) ))

		pkg['cmd'] = cmd

		''' Try running the commands '''
		try:
			cmd_issue.setup(cmd)
		except Exception, e:
			raise Exception('Could not attach zone to the exit interface successfully: %s' % (str(e)))

		self.pkg = pkg

		return

	def detach2exit(self, pkg):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.detach2exit')

		try:
			cmd_issue.teardown(pkg['cmds'])
		except Exception, e:
			raise Exception('Could not detach from exit interface successfully: %s' % (str(e)))

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping threads created in zone_init')
		for thread in self.threads:
			thread.terminate()

		logger.debug('Removing zone network hooks')
		self.detach2exit(self.pkg)


'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':

	import time

	LOG_FMT_CON		= '%(name)s - %(levelname)s - %(message)s'

	logger = logging.getLogger('pktjumble')
	logger.setLevel(logging.DEBUG)
	consoleHandler = logging.StreamHandler()
	logger.addHandler(consoleHandler)
	con_format = logging.Formatter(fmt=LOG_FMT_CON)
	consoleHandler.setFormatter(con_format)
	logger.debug('Running zone test!')

	myzone_obj = zone('UID-XXXXXXX')
		
	gates = myzone_obj.gates

	lgates, rgates = gates
	router_gates = {}
	
	# Creating custom gate queues
	for i in range((lgates+rgates)):
								
		q1 = Queue()
		q2 = Queue()

		q_pair = (q1, q2)

		if i > lgates:
			router_gates[('right_' + str(i - lgates))] = q_pair
		else:
			router_gates[('left_' + str(i))] = q_pair

	myzone_obj.start(router_gates)
	
	logger.debug('Zone created and running.')

	time.sleep(5)

	# Job has completed, exit
	myzone_obj.stop()

'''
END OF EXAMPLE
'''
