from lib.parent_zone import parent_zone

import avail_zones.simple_bridge.config as zconf

import logging, logging.handlers, sys, os
from multiprocessing import Process, Queue

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		# Pull out the specific queues we need. For both sides
		left_zone_input, left_zone_output = zone_gates['left_0']
		right_zone_input, right_zone_output = zone_gates['right_0']

		# Create threads for each of the gates, both recv/send
		self.left_gate_th = Process(target=self.left_gate, args=(left_zone_input, right_zone_output,))
		self.left_gate_th.daemon = True
		self.left_gate_th.start()

		self.right_gate_th = Process(target=self.right_gate, args=(right_zone_input, left_zone_output,))
		self.right_gate_th.daemon = True
		self.right_gate_th.start()

	def left_gate(self, left_zone_input, right_zone_output):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'left_gate')

		while True:
			logger.debug('Waiting for packets')
			pkt = left_zone_input.get()
			
			logger.debug('Got packet')

			right_zone_output.put(pkt)
			logger.debug('Put onto right_zone_output')

	def right_gate(self, right_zone_input, left_zone_output):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'right_gate')
		
		while True:
			logger.debug('Waiting for packets')
			pkt = right_zone_input.get()

			logger.debug('Got packet')

			left_zone_output.put(pkt)
			logger.debug('Put onto left_zone_output')

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping threads created in zone_init')
		self.left_gate_th.terminate()
		self.right_gate_th.terminate()

'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':
	pass

'''
END OF EXAMPLE
'''
