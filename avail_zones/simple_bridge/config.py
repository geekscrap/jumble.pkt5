''' Zone meta '''
long_name		= 'Simple bridge'
category		= 'BRIDGE;TRANSIT'
author			= 'Matthew Holley'
version			= 1.0
description		= 'This is an example module that can be customised to manipulate packets as part of a path'

''' Zone attributes '''
name			= 'simple_bridge'
gates			= (1,1)
buffer			= True
args			= None

'''
Checklist when creating a custom zone module:
	MUST	-	Class name - The main class name must be 'zone' to be searchable for import.

	MUST	-	__init__ - Set the 'gates' variable. Format is an array based off two
				fields, the number of gates required on the left and similarly, on the right.
				For example if 2 left and 3 right are required the variable is set to (2,3).

	MUST	-	Contain a zone_init function - This function includes code to setup the zone
				in its entirety. This includes creation and starting of threads to manage
				packets being received from the in/output queue's. Allocated queues for the
				gates are found within the zone_gates dictionary. The example shows how to access
				these individual queues. Allocations by number start at 1, NOT 0. This is
				also the area to setup and start creation of namespace networks.

	MUST	-	zone_cleanup - Include a function named 'zone_cleanup'. This zone is responsible for
				teardown of any started or created threads, namespace networks and etc. that have
				been explicitly created within this child module.

	MUST	-	Always set any threads/processes that are created as daemons

	MUST	-	Set objects as self.x if referenced across zone_init and zone_cleanup.
				For example, new process in zone_init and start/terminate in zone_cleanup.

	MUST	-	Functions should be created to retrieve packets off of the queues and started as a
				process from within zone_init. These should be customised to reflect the zones purpose.
				The example below acts as a very simple bridge. Packets come in	on the left or
				right side and are forwarded directly to the opposite side via the send queue for
				delivery back into the system.
				
	OPTION	-	Set the buffer flag in the __init__ function argument. Setting buffer to False
				means that the zone will not use inbuilt queuing functions, and therefore the zone
				is responsible for throughput. However, it also speeds up the input/output of the
				zone as there is one less queue for the packet to be put through.
				
	OPTION	-	Zones can retrieve arguments specifically for them by accessing the self.args variable.
				This can be used for such things as timers, delays, executable name declarations etc.
				The zone creator is responsible for parsing these arguments (in dict form).

'''
