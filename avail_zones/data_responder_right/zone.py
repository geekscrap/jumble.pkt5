from lib.parent_zone import parent_zone
import avail_zones.data_responder_right.config as zconf

import logging, logging.handlers, sys, os, time
from multiprocessing import Process, Queue

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)
		
		# Set the zone variables below
		# Delay between receiving data and responding
		self.delay = 0

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		# Pull out the specific queues we need. For both sides
		left_router_input, left_router_output = zone_gates['left_1']
		
		# We need a queue to switch between threads
		loop_q = Queue()

		self.recv_request_th = Process(target=self.recv_request, args=(left_router_input, left_router_output,))
		self.recv_request_th.daemon = True
		self.recv_request_th.start()

	def recv_request(self, left_router_input, left_router_output,):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'recv_request')

		while True:
			logger.debug('Waiting for data from router queue')
			data = str(left_router_input.get())
			logger.debug('Got data from router: ' + data)
			time.sleep(self.delay)
			response = data + '-REPLY'
			logger.debug('Sending response to router: ' + response)
			left_router_output.put(response)

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping threads created in zone_init')
		self.recv_request_th.terminate()

'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':

	logger = logging.getLogger('pktjumble')
	logger.setLevel(logging.DEBUG)
	socketHandler = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
	logger.addHandler(socketHandler)
	logger.debug('Running zone test!')

	import time, random

	myzone_obj = zone('UID-XXXXXXX')
		
	gates = myzone_obj.gates

	lgates, rgates = gates
	router_gates = {}
	
	# Creating custom gate queues
	for i in range(1, (lgates+rgates)+1):
								
		q1 = Queue()
		q2 = Queue()

		q_pair = (q1, q2)

		if i > lgates:
			router_gates[('right_' + str(i - lgates))] = q_pair
		else:
			router_gates[('left_' + str(i))] = q_pair

	# Run the zones and its threads
	# If this statement blocks, the zone will not work. Assess the threads
	myzone_obj.start(router_gates)
	
	print 'Zone created and running.'

	left_router_output, left_router_input = router_gates['left_1']


	session_id = str(random.randrange(10000,99999))
	logger.debug('Starting sending data in 3 seconds. Session ID: ' + session_id)

	for i in range(1, 11):
		pkt = 'SID=' + session_id + ',PID=' + str(i)
		left_router_output.put(pkt)
		print 'Sent data to zone: ' + pkt
		data = str(left_router_input.get())
		print 'Router got some data: ' + data
		time.sleep(1)

	# Job has completed, exit
	myzone_obj.stop()

'''
END OF EXAMPLE
'''
