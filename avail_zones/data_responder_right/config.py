''' Zone meta '''
long_name		= 'Data responder right'
category		= 'TEST;DATA;EXIT'
author			= 'Matthew Holley'
version			= 1.0
description		= 'A zone that can be used in tandem with data_gen_left to test other zones and check routing functionality. Cannot be used in real scenario.'

''' Zone attributes '''
name			= 'data_responder_right'
gates			= (1,0)
buffer			= True
args			= None
