''' Zone meta '''
long_name		= 'Data generator left'
category		= 'TEST;DATA;GENERATOR'
author			= 'Matthew Holley'
version			= 1.0
description		= 'A zone that can be used to test (in conjunction with data_responder_right) other zones performance or to test correct routing of zones. Cannot be used in real scenario.'

''' Zone attributes '''
name			= 'data_gen_left'
gates			= (0,1)
buffer			= True
args			= None
