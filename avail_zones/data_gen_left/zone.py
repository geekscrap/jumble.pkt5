from lib.parent_zone import parent_zone
import avail_zones.data_gen_left.config as zconf

import logging, logging.handlers, sys, os
from multiprocessing import Process, Queue

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)
		
		# Set the zone variables below
		
		# How many packets of data to send
		self.amount = 10
		# Time between sending data
		self.sleep_interval = 1

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		# Pull our the specific queues we need. For both sides
		right_router_output, right_router_input = zone_gates['right_1']

		self.gen_data_th = Process(target=self.gen_data, args=(right_router_input,))
		self.gen_data_th.daemon = True
		self.gen_data_th.start()

		self.recv_response_th = Process(target=self.recv_response, args=(right_router_output,))
		self.recv_response_th.daemon = True
		self.recv_response_th.start()

	def gen_data(self, right_router_input):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'gen_data')

		import time, random

		session_id = str(random.randrange(10000,99999))
		logger.debug('Starting sending data in 3 seconds. Session ID: ' + session_id)
		time.sleep(3)

		for i in range(1, self.amount+1):
			time.sleep(self.sleep_interval)
			pkt = 'SID=' + session_id + ',PID=' + str(i)
			right_router_input.put(pkt)
			logger.debug('Sent data: ' + pkt)

	def recv_response(self, right_router_output):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'recv_response')

		while True:
			logger.debug('Waiting for data from router queue')
			pkt = right_router_output.get()
			logger.debug('Got data from router: ' + str(pkt))

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping threads created in zone_init')
		self.gen_data_th.terminate()
		self.recv_response_th.terminate()

'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':

	logger = logging.getLogger('pktjumble')
	logger.setLevel(logging.DEBUG)
	socketHandler = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
	logger.addHandler(socketHandler)
	logger.debug('Running zone test!')

	import time

	myzone_obj = zone('UID-XXXXXXX')
		
	gates = myzone_obj.gates

	lgates, rgates = gates
	router_gates = {}
	
	# Creating custom gate queues
	for i in range(1, (lgates+rgates)+1):
								
		q1 = Queue()
		q2 = Queue()

		q_pair = (q1, q2)

		if i > lgates:
			router_gates[('right_' + str(i - lgates))] = q_pair
		else:
			router_gates[('left_' + str(i))] = q_pair

	myzone_obj.start(router_gates)
	print 'Zone created and running.'

	right_router_output, right_router_input = router_gates['right_1']

	for i in range(1, myzone_obj.amount+1):
		print 'Router is waiting for data'
		data = str(right_router_input.get())
		print 'Router got some data: ' + data
		response = data + '-REPLY'
		print 'Responding to zone with: ' + response
		right_router_output.put(response)
	
	# Job has completed, exit
	myzone_obj.stop()

'''
END OF EXAMPLE
'''
