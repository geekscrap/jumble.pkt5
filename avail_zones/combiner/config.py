''' Zone meta '''
long_name		= 'Combiner'
category		= 'TEST;DATA;MERGER'
author			= 'Matthew Holley'
version			= 1.0
description		= 'This is an example module that that combines its input based on string values. Not useful in the real world!'

''' Zone attributes '''
name			= 'combiner'
gates			= (2,1)
buffer			= True
args			= None