from lib.parent_zone import parent_zone

import avail_zones.example.config as zconf

import logging, logging.handlers, sys, os
from multiprocessing import Process, Queue

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		# Pull out the specific queues we need. For both sides
		left_zone_input, left_zone_output = zone_gates['left_1']
		right_zone_input, right_zone_output = zone_gates['right_1']

		# Create threads for each of the gates, both recv/send
		self.left_gate_th = Process(target=self.left_gate, args=(left_zone_input, right_zone_output,))
		self.left_gate_th.daemon = True
		self.left_gate_th.start()

		self.right_gate_th = Process(target=self.right_gate, args=(right_zone_input, left_zone_output,))
		self.right_gate_th.daemon = True
		self.right_gate_th.start()

	def left_gate(self, left_zone_input, right_zone_output):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'left_gate')

		while True:
			logger.debug('Waiting for packets')
			pkt = left_zone_input.get()
			logger.debug('Got: ' + str(pkt))
			right_zone_output.put(pkt)
			logger.debug('Put onto right_zone_output')

	def right_gate(self, right_zone_input, left_zone_output):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'right_gate')
		
		while True:
			logger.debug('Waiting for packets')
			pkt = right_zone_input.get()
			logger.debug('Got: ' + str(pkt))
			left_zone_output.put(pkt)
			logger.debug('Put onto left_zone_output')

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping threads created in zone_init')
		self.left_gate_th.terminate()
		self.right_gate_th.terminate()

'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':

	LOG_FMT_CON		= '%(name)s - %(levelname)s - %(message)s'

	logger = logging.getLogger('pktjumble')
	logger.setLevel(logging.DEBUG)
	consoleHandler = logging.StreamHandler()
	logger.addHandler(consoleHandler)
	con_format = logging.Formatter(fmt=LOG_FMT_CON)
	consoleHandler.setFormatter(con_format)
	logger.debug('Running zone test!')

	import time

	myzone_obj = zone('UID-XXXXXXX')

	gates = myzone_obj.gates

	lgates, rgates = gates
	router_gates = {}
	
	# Creating custom gate queues
	for i in range(1, (lgates+rgates)+1):
								
		q1 = Queue()
		q2 = Queue()

		q_pair = (q1, q2)

		if i > lgates:
			router_gates[('right_' + str(i - lgates))] = q_pair
		else:
			router_gates[('left_' + str(i))] = q_pair

	# Run the zones and its threads
	# If this statement blocks, the zone will not work. Assess the threads
	myzone_obj.start(router_gates)
	
	print 'Zone created and running.'

	left_router_output, left_router_input = router_gates['left_1']
	right_router_output, right_router_input = router_gates['right_1']

	msg = 'HELLO WORLD'
	
	while True:
		# Send data across the zone and receive it
		print 'Sending data:\t' + str(msg)
		left_router_output.put(msg)
		print 'Got data:\t' + str(right_router_input.get())

	# Job has completed, exit
	myzone_obj.stop()

'''
END OF EXAMPLE
'''
