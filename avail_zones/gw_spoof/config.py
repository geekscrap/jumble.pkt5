''' Zone meta '''
long_name		= 'Gateway spoofing zone'
category		= 'ENTRY;SCAPY;MITM'
author			= 'Matthew Holley'
version			= 1.0
description		= 'This zone spoofs the network gateway - Captures packets to send though the system.'

''' Zone attributes '''
name			= 'gw_spoof'
gates			= (0,1)
buffer			= True
args			= {'iface': 'UNTR_BR', 'bpf': None}