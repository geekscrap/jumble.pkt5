import logging, logging.handlers, sys, os, commands
from multiprocessing import Process, Queue

from lib.parent_zone import parent_zone
from lib.sr import sr

import avail_zones.gw_spoof.config as zconf
from lib.const import TRUST_NAT_IP, UNTRUST_GW_IP

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		right_zone_input, right_zone_output = zone_gates['right_0']

		''' Setup of packet capture thread '''
		iface = zconf.args['iface']

		''' BPF currently not implemented
		bpf = zconf.args['bpf']

 		if bpf == None:
			bpf = 'ether dst ' + srcmac + ' and ip'
 		else:
 			bpf = '( ' + bpf + ' ) and (ether dst ' + srcmac + ' and ip)'

 		logger.info('Running with the following, sniffing on: ' + iface + ', with bpf: ' + bpf)
 		'''

 		self.sniff = sr(iface, right_zone_output, right_zone_input)
 		self.sniff.start()

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping sr function created in zone_init')
		self.sniff.stop()


'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':

	from scapy.layers.all import Ether, Raw, IP, ICMP

	LOG_FMT_CON		= '%(name)s - %(levelname)s - %(message)s'

	logger = logging.getLogger('pktjumble')
	logger.setLevel(logging.DEBUG)
	consoleHandler = logging.StreamHandler()
	logger.addHandler(consoleHandler)
	con_format = logging.Formatter(fmt=LOG_FMT_CON)
	consoleHandler.setFormatter(con_format)
	logger.debug('Running zone test!')

	import time

	myzone_obj = zone('UID-XXXXXXX')

	gates = myzone_obj.gates

	lgates, rgates = gates
	router_gates = {}

	q1 = Queue()
	q2 = Queue()

	q3 = Queue()
	q4 = Queue()

	router_gates = {'right_0':(q1,q2), 'left_0':(q3,q4)}

	myzone_obj.start(router_gates)
	logger.debug('Zone created and running.')

	right_router_output, right_router_input = router_gates['right_0']

	while True:
		logger.debug('Waiting for pkt')
		pkt = right_router_input.get()
		logger.debug(IP(pkt).summary())


	logger.debug('Stopping!')

	# Job has completed, exit
	myzone_obj.stop()
'''
END OF EXAMPLE
'''
