''' Zone meta '''
long_name		= 'Splitter'
category		= 'TEST;DATA;TRANSIT;LOGIC'
author			= 'Matthew Holley'
version			= 1.0
description		= 'This is an example module that that splits its input based on string values. Not useful in the real world!'

''' Zone attributes '''
name			= 'splitter'
gates			= (1,2)
buffer			= True
args			= None
