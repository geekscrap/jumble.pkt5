from lib.parent_zone import parent_zone
import avail_zones.splitter.config as zconf

import logging, logging.handlers, sys, os
from multiprocessing import Process, Queue

class zone(parent_zone):

	def __init__(self, uid, args=zconf.args):
		parent_zone.__init__(self, uid, args, zconf.gates, zconf.name, zconf.buffer)

	def zone_init(self, zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_init')

		# Pull out the specific queues we need. For both sides
		left_1_zone_input, left_1_zone_output = zone_gates['left_1']
		right_1_zone_input, right_1_zone_output = zone_gates['right_1']
		right_2_zone_input, right_2_zone_output = zone_gates['right_2']

		# Create threads for each of the gates, both recv/send
		self.left_1_gate_th = Process(target=self.left_1_gate, args=(left_1_zone_input, right_1_zone_output, right_2_zone_output,))
		self.left_1_gate_th.daemon = True
		self.left_1_gate_th.start()

		self.right_1_gate_th = Process(target=self.right_gate, args=(right_1_zone_input, left_1_zone_output,))
		self.right_1_gate_th.daemon = True
		self.right_1_gate_th.start()

		self.right_2_gate_th = Process(target=self.right_gate, args=(right_2_zone_input, left_1_zone_output,))
		self.right_2_gate_th.daemon = True
		self.right_2_gate_th.start()

	def left_1_gate(self, left_1_zone_input, right_1_zone_output, right_2_zone_output):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'left_1_gate')

		while True:
			logger.debug('Waiting for packets')
			pkt = left_1_zone_input.get()
			logger.debug('Got: ' + str(pkt))
			
			pid = pkt.find('=', 5)
			pid = int(pkt[(pid+1):])
			
			if pid % 2 == 0:
				right_1_zone_output.put(pkt)
				logger.debug('Put onto right_1_zone_output')
			else:
				right_2_zone_output.put(pkt)
				logger.debug('Put onto right_2_zone_output')

	def right_gate(self, input, ouput):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'right_gates')
		
		while True:
			logger.debug('Waiting for packets')
			pkt = input.get()
			logger.debug('Got: ' + str(pkt))
			ouput.put(pkt)
			logger.debug('Put onto left_zone_output')

	def zone_cleanup(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + 'zone_cleanup')
		
		logger.debug('Stopping threads created in zone_init')
		self.left_1_gate_th.terminate()
		self.right_1_gate_th.terminate()
		self.right_2_gate_th.terminate()

'''
For testing purposes when creating a new zone
'''
if __name__ == '__main__':

	logger = logging.getLogger('pktjumble')
	logger.setLevel(logging.DEBUG)
	socketHandler = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
	logger.addHandler(socketHandler)
	logger.debug('Running zone test!')

	import time, random

	myzone_obj = zone('UID-XXXXXXX')

	gates = myzone_obj.gates

	lgates, rgates = gates
	router_gates = {}
	
	# Creating custom gate queues
	for i in range(1, (lgates+rgates)+1):
								
		q1 = Queue()
		q2 = Queue()

		q_pair = (q1, q2)

		if i > lgates:
			router_gates[('right_' + str(i - lgates))] = q_pair
		else:
			router_gates[('left_' + str(i))] = q_pair

	# Run the zones and its threads
	# If this statement blocks, the zone will not work. Assess the threads
	myzone_obj.start(router_gates)
	
	print 'Zone created and running.'

	left_1_router_output, left_1_router_input = router_gates['left_1']
	right_1_router_output, right_1_router_input = router_gates['right_1']
	right_2_router_output, right_2_router_input = router_gates['right_2']

	session_id = str(random.randrange(10000,99999))
	print 'Starting sending data in 3 seconds. Session ID: ' + session_id

	amount = 10
	print '\n*** Sending data from left_1'
	for i in range(0, amount+1, 2):
		pkt = 'SID=' + session_id + ',PID=' + str(i)
		left_1_router_output.put(pkt)
		print 'Sent data on left_1: ' + pkt
		pkt = right_1_router_input.get()
		print 'Got data back on right_1: ' + pkt

	for i in range(1, amount+1, 2):
		pkt = 'SID=' + session_id + ',PID=' + str(i)
		left_1_router_output.put(pkt)
		print 'Sent data on left_1: ' + pkt
		pkt = right_2_router_input.get()
		print 'Got data back on right_2: ' + pkt
		
	print '\n*** Sending data from right_1'
	for i in range(0, amount+1, 1):
		pkt = 'SID=' + session_id + ',PID=' + str(i) + '-REPLY'
		right_1_router_output.put(pkt)
		print 'Sent data on right_1: ' + pkt
		pkt = left_1_router_input.get()
		print 'Got data back on left_1: ' + pkt

	print '\n*** Sending data from right_2'
	for i in range(0, amount+1, 1):
		pkt = 'SID=' + session_id + ',PID=' + str(i) + '-REPLY'
		right_2_router_output.put(pkt)
		print 'Sent data on right_2: ' + pkt
		pkt = left_1_router_input.get()
		print 'Got data back on left_1: ' + pkt

	# Job has completed, exit
	myzone_obj.stop()

'''
END OF EXAMPLE
'''
