#!/usr/bin/env python

import sys, logging, logging.handlers, os, signal, time, argparse
from multiprocessing import Process

from lib import cmd_issue, zone_manager, log_receiver, json_parser
from lib.const import DEBUG, INFO, ZONE_CONF, LOG_DIR, LOG_FMT_CON, LOG_FMT_FILE, NET_SETUP

''' Get arguments '''
parser = argparse.ArgumentParser(description='')

parser.add_argument('-w', '--wgui', help='Run with Web GUI', action='store_true')
parser.add_argument('-z', '--zoneconf', help='Zone json configuration file')
parser.add_argument('-l', '--logdir', help='Set log directory')
parser.add_argument('-d', '--debug', help='Run in debug mode', action='store_true')

group = parser.add_mutually_exclusive_group()
group.add_argument('-ne', '--noenv', help='Dont run the environment setup script, but run the app', action='store_true')
group.add_argument('-es', '--envsetup', help='ONLY run the environment setup script and exit', action='store_true')
group.add_argument('-etd', '--envteardown', help='ONLY run the environment teardown script and exit', action='store_true')
group.add_argument('-eftd', '--envforceteardown', help='ONLY run the environment teardown script FORCEFULLY and exit', action='store_true')

args = parser.parse_args()

''' Define results of given arguments '''
if args.debug:
	loglevel = DEBUG
else:
	loglevel = INFO
# DEBUGGING ONLY REMOVE IN PROD
loglevel = DEBUG

if args.zoneconf is not None:
	ZONE_CONF = args.zoneconf

if args.logdir is not None:
	LOG_DIR = args.logdir


''' Create the logger '''
log_th = Process(target=log_receiver.local_log, args=(LOG_DIR,LOG_FMT_FILE,))
log_th.start()
''' Let the logger start '''
time.sleep(0.5)

logger = logging.getLogger('pktjumble')
logger.setLevel(loglevel)
socketHandler = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
consoleHandler = logging.StreamHandler()
con_format = logging.Formatter(fmt=LOG_FMT_CON)
consoleHandler.setFormatter(con_format)

logger.addHandler(socketHandler)
logger.addHandler(consoleHandler)


''' Run if we only want to setup/teardown the app environment (envsetup/envteardown options) '''
if args.envsetup:
	try:
		cmd_issue.setup(NET_SETUP)
	except Exception, error:
		logger.critical(str(error))
		sys.exit(1)
	sys.exit(0)

if args.envteardown or args.envforceteardown:
	try:
		if args.envforceteardown:
			cmd_issue.teardown(NET_SETUP, True)
		else:
			cmd_issue.teardown(NET_SETUP)
	except Exception, error:
		logger.critical(str(error))
		sys.exit(1)
	sys.exit(0)

''' Setup the environment for the app to work in '''
if not args.noenv:
	try:
		cmd_issue.setup(NET_SETUP)
	except Exception, error:
		logger.critical(str(error))
		sys.exit(1)

''' Choose the run mode, web gui or console '''
if args.wgui:
	''' Run via WebGUI '''
	logger = logging.getLogger('pktjumble.run.wgui')

	from lib import flask_manager

	logger.info('Starting Flask manager')
	flask = flask_manager.manage()
	logger.info('View the command index at: ')

	flask.start()

	logger.info('Flask manager stopped, terminating')
	time.sleep(0.5)

else:
	''' Run via CLI '''
	logger = logging.getLogger('pktjumble.run.cli')

	''' Catch ^C signal '''
	def sig_catch(signal, frame):
		logger = logging.getLogger('pktjumble.run.cli.sig_catch')

		global no_kill
		no_kill = False
		logger.info('Got ^C - Quitting... Please wait...')

	''' Load zones configuration '''
	json_str = json_parser.file2json(ZONE_CONF)
	zone_conf_dict = json_parser.json2dict(json_str)

	if not zone_conf_dict:
		logger.info('Error parsing ' + ZONE_CONF + ', exiting.')
		sys.exit(1)

	''' Start the zone manager '''
	cli = zone_manager.manage()
	logger.info('Starting zones')

	try:
		cli.start(zone_conf_dict)

		''' Catch the SIGINT to stop the process '''
		signal.signal(signal.SIGINT, sig_catch)

		logger.info('Waiting for ^C')

		no_kill = True
		while no_kill:
			signal.pause()

		logger.info('Stopping zones')
		cli.stop()
		logger.info('Zones stopped, terminating')

	except Exception, e:
		logger.error('Error starting zones: %s' % e)	

	time.sleep(0.5)

''' Tear down the setup we did earlier '''
if not args.noenv:
	try:
		cmd_issue.teardown(NET_SETUP)
	except Exception, e:
		logger.critical(str(e))

# PERHAPS MAKE THIS A DAEMON THREAD?
log_th.terminate()
time.sleep(1)

print 'Done .'