**Work in progress!**

Scripts and VM to allow modification, anonymisation and capture of packets when used as a gateway for your local network. Can be used for malware analysis, generic anonymisation etc.

Options: - Use of a Tor transparent proxy (pushes ALL traffic through Tor) - SSLproxy to allow inspection of traffic for malware for analysis - Capture of network traffic on the gateway - Replace text within packets...

Path instances can consist of multiple elements and directional flows, for example:

![pkt-jam.png](https://bitbucket.org/repo/ge6g8X/images/3033638710-pkt-jam.png)