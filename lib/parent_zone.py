from multiprocessing import Process, Queue
import logging, logging.handlers, signal

from lib import cmd_issue
from lib.const import UNTRUST_NW, UNTRUST_NMASK, UNTRUST_GW_IP, EXIT_BR

class parent_zone:

	def __init__(self, uid, args, gates, name, buffer=True):
		signal.signal(signal.SIGINT, self.gotkill)

		self.uid = uid
		self.args = args
		self.gates = gates
		self.name = name
		self.buffer = buffer

		self.ident = '[ UID: ' + str(self.uid) + ', TYPE: ' + self.name + ' ]'
		
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.init')
		logger.info('Initalization beggining')

		logger.info('Initiating zone ' + self.ident + ', ARGS: [ ' + str(self.args) + ' ]')
		logger.info('Zone queue buffer set to ' + str(buffer))

		self.gate_th_list = []
		self.zone_gates = {}

		''' Test if this zone wants to use bpf filters
		if 'bpf' in args:
			with os.popen("tcpdump -V 2> /dev/null") as _f:
				if _f.close() >> 8 == 0x7f:
					log_loading.warning("Failed to execute tcpdump. Check it is installed and in the PATH")
					TCPDUMP=0
				else:
					TCPDUMP=1
			del(_f)
		'''

		''' Check that all the required functions are provided in the child module '''
		check_functions = ['zone_init', 'zone_cleanup']
		logger.debug('Checking functions exist in child module: ' + str(check_functions))
		self.check_functions_error = False
		for func in check_functions:
			if not func in dir(self):
				logger.error('\'' + str(func) + '\' doesn\'t exist in child module')
				self.check_functions_error = True
		if self.check_functions_error:
			logger.error('Required functions dont exist. Exiting - see above for those that are required')
			raise Exception('Required functions dont exist. Exiting - see above for those that are required')

	def gotkill(self, a,b):
		pass

	def start(self, alloc_zone_gates):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.start')
		
		if self.check_functions_error:
			logger.error('Not running zone, error within custom function')
			raise Exception('Required functions dont exist. Exiting - see above for those that are required')

		logger.info('Running zone: ' + self.uid)

		''' Test if a zone buffer is required '''
		if not self.buffer:
			logger.info('Running child zone_init function WITHOUT zone buffer')
			try:
				self.zone_init(alloc_zone_gates)
				return
			except Exception, error:
				logger.error('Error occurred in zone_init function in child module, Exiting. Error: %s' % str(error))
				raise Exception('Error occurred in zone_init function in child module, Exiting. Error: %s' % str(error))

		lgates, rgates = self.gates

		logger.debug('Creating zone left/right, input/output gate threads and assigning queues - ' + str(lgates) + ' left, ' + str(rgates) + ' right')
		for i in range(lgates):

			logger.debug('Assigning left queues: ' + str(i))

			child_zone_input = Queue()
			child_zone_output = Queue()

			zone_input, zone_output = alloc_zone_gates[('left_' + str(i))]

			zone_input_th = Process(target=self.zone_buffer, args=(('buffer-l_zone_input_' + str(i)), zone_input, child_zone_input,))
			self.gate_th_list.append((zone_input_th, zone_input))
			zone_output_th = Process(target=self.zone_buffer, args=(('buffer-l_zone_output_' + str(i)), child_zone_output, zone_output,))
			self.gate_th_list.append((zone_output_th, child_zone_output))

			gate_q_pair = (child_zone_input, child_zone_output)
			self.zone_gates[('left_'+str(i))] = gate_q_pair

		for i in range(rgates):

			logger.debug('Assigning right queue: ' + str(i))
	
			child_zone_input = Queue()
			child_zone_output = Queue()
		
			zone_input, zone_output = alloc_zone_gates[('right_' + str(i))]

			zone_input_th = Process(target=self.zone_buffer, args=(('buffer-r_zone_input_' + str(i)), zone_input, child_zone_input,))
			self.gate_th_list.append((zone_input_th, zone_input))
			zone_output_th = Process(target=self.zone_buffer, args=(('buffer-r_zone_output_' + str(i)), child_zone_output, zone_output,))
			self.gate_th_list.append((zone_output_th, child_zone_output))

			gate_q_pair = (child_zone_input, child_zone_output)
			self.zone_gates[('right_'+str(i))] = gate_q_pair

		logger.info('Running child zone_init function')
		try:
			self.zone_init(self.zone_gates)
		except Exception, error:
			logger.error('Error occurred in zone_init function in child module, Exiting. Error: %s' % str(error))
			raise Exception('Error occurred in zone_init function in child module, Exiting. Error: %s' % str(error))

		logger.debug('Starting zone input/output gate threads')
		for th, q in self.gate_th_list:
			th.start()

	def zone_buffer(self, queue_name, q1, q2):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.' + queue_name)
		signal.signal(signal.SIGINT, self.gotkill)

		try:
			for pkt in iter(q1.get, None):
				logger.debug('Packet buffered: ' + str(pkt.encode('hex')))
				q2.put(pkt)
		except Exception, e:
			pass
			
		logger.debug('Quitting thread, stopping forwarding')

	def stop(self):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.stop')

		logger.info('Stopping zone ' + self.ident)

		if self.buffer:
			logger.debug('Sending kill to input/output threads')
			for th, q in self.gate_th_list:
				q.put(None)

				''' Wait for threads to die '''
				if th.is_alive():
					th.join(0.5)
					''' If threads are still alive, forcefully kill them '''
					if th.is_alive():
						logger.error('Terminating thread - didn\'t join() in time')
						th.terminate()

		try:
			logger.info('Running child zone_cleanup function')
			self.zone_cleanup()
		except Exception, error:
			logger.debug('Error occurred in zone_init function in child module, already exiting. Error: ' + str(error))

		logger.info('Quitting zone')

	''' Custom functions '''
	def setns(self, nsname):
		logger = logging.getLogger('pktjumble.zone.' + self.uid + '.setns')

		logger.debug('Sending process to namespace: ' + nsname)

		netnspath = '%s%s' % ('/run/netns/', nsname)
		netnspath = netnspath.encode('ascii')

		import ctypes
		libc = ctypes.CDLL('libc.so.6')

		CLONE_NEWNET = 0x40000000

		fd = open(netnspath)
		ret = libc.syscall(308, fd.fileno(), CLONE_NEWNET)

		if ret != 0:
			raise Exception('syscall failed. Exit code: ' + str(ret))
		logger.debug('Sucessfully placed process into namespace')
