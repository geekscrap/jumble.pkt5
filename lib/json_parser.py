import logging, logging.handlers, json

from lib.const import ZNAME, GATES, ZARGS

def json2dict(json_str):
	logger = logging.getLogger('pktjumble.json_parser.json2dict')
	
	def _decode_list(data):
		rv = []
		for item in data:
			if isinstance(item, unicode):
				item = item.encode('utf-8')
			elif isinstance(item, list):
				item = _decode_list(item)
			elif isinstance(item, dict):
				item = _decode_dict(item)
			rv.append(item)
		return rv
	def _decode_dict(data):
		rv = {}
		for key, value in data.iteritems():
			if isinstance(key, unicode):
				key = key.encode('utf-8')
				if key.isdigit():
					key = int(key)
			if isinstance(value, unicode):
				value = value.encode('utf-8')
			elif isinstance(value, list):
				value = _decode_list(value)
			elif isinstance(value, dict):
				value = _decode_dict(value)
			rv[key] = value
		return rv

	logger.debug('Loading dict from json')

	try:
		converted_dict = json.loads(json_str, object_hook=_decode_dict)
	except Exception, error:
		logger.error('Error occurred while converting json to dictionary. Error: ' + str(error))
		return False

	logger.debug('Converted json ok')

	return converted_dict

def file2json(json_file):
	logger = logging.getLogger('pktjumble.json_parser.file2json')

	try:

		json_f = open(json_file)
		logger.debug('Opened json file')

		json_str = ''

		for line in json_f:
			json_str += line

	except Exception, error:
		logger.error('Error occurred while loading json from file. Error: ' + str(error))
		json_f.close()
		return False

	return json_str
