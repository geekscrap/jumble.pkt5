import logging, logging.handlers
from multiprocessing import Process, Queue

from lib import plugin_manager
from lib.const import ZNAME, GATES, ZOBJ, ZARGS

class manage:

	def __init__(self):
		logger = logging.getLogger('pktjumble.zone_manager.init')

		logger.info('Loading available zone plugins')
		self.available_zones = plugin_manager.load_zones()

		self.loaded_zones = {}
		self.zone_conf_dict = None

	def start(self, zone_conf_dict):
		logger = logging.getLogger('pktjumble.zone_manager.start')

		logger.debug('Initiating zones')

		self.zone_conf_dict = zone_conf_dict
		
		queue_dict = {}
		add_zone_obj = {}

		for uid, zone in self.zone_conf_dict.items():

			zone_name_attrib	= zone[ZNAME]
			zone_gates_attrib	= zone[GATES]
			zone_zargs_attrib	= zone[ZARGS]

			if zone_name_attrib not in self.available_zones:
				logger.critical(zone_name_attrib + ' zone is not available. Does it exist in enabled_zones directory?')
				raise Exception('Zone not available: %s' % zone_name_attrib)
			
			''' Assign the zone object to the zone dict '''
			zone_pkg = self.available_zones[zone_name_attrib]
			
			''' Test to see if user has set arguments that overwrite those set in zone's config.py '''
			zargs_frm_config = self.available_zones[zone_name_attrib]

			if (zone_zargs_attrib == None) or (zargs_frm_config.zconf.args == None):
				zone_obj = zone_pkg.zone(uid)
			else:
				args_override = {}
				for arg, config_set_value in zargs_frm_config.zconf.args.items():
					if arg in zone_zargs_attrib:
						args_override[arg] = zone_zargs_attrib[arg]
					else:
						args_override[arg] = config_set_value

				zone_obj = zone_pkg.zone(uid, args_override)

			zone_obj.uid = uid
			add_zone_obj[uid] = zone_obj

			''' Allocate the queues to zones '''
			gate_to_modify = None
			pair_to_swap = None
			
			for gate_name, gate_pair in zone_gates_attrib.items():

				if gate_pair == False:
					continue

				l_gate, r_gate = gate_pair

				if l_gate in queue_dict:
					l_gate = queue_dict.get(l_gate)
				else:
					queue_dict[l_gate] = Queue()
					l_gate = queue_dict.get(l_gate)

				if r_gate in queue_dict:
					r_gate = queue_dict.get(r_gate)
				else:
					queue_dict[r_gate] = Queue()
					r_gate = queue_dict.get(r_gate)
				
				zone_gates_attrib[gate_name] = (l_gate, r_gate)

		''' Add new zone object to the zone config dict'''
		for uid, zone_config_attribs in add_zone_obj.items():
			zone = self.zone_conf_dict.pop(uid)
			zone[ZOBJ] = zone_config_attribs
			self.zone_conf_dict[uid] = zone

		logger.info('Starting zones')
		for uid, zone_config_attribs in self.zone_conf_dict.items():
			zone_obj = zone_config_attribs[ZOBJ]

			try:
				zone_obj.start(zone_config_attribs[GATES])
				logger.info('Started zone with UID: ' + uid + '. Zone type: ' + str(zone_config_attribs[ZNAME]))
			except Exception, e:
				zone_obj.stop()
				raise Exception('Error starting zone with UID: ' + uid + ', Zone type: ' + str(zone_config_attribs[ZNAME]))

		logger.debug('Running zone config: ' + str(self.zone_conf_dict))

	def stop(self):
		logger = logging.getLogger('pktjumble.zone_manager.stop')

		logger.debug('Got stop command, stopping zones')
		for uid, zone_config_attribs in self.zone_conf_dict.items():

			zone_obj = zone_config_attribs[ZOBJ]
			zone_obj.stop()

	def get_available_zones(self):
		logger = logging.getLogger('pktjumble.zone_manager.get_zone_attribs')

		logger.info('Retreiving zone details')
		zone_attribs = {}

		for zone_name, obj in self.available_zones.items():
			zone_attribs[zone_name] = {
					'meta':	{
						'long_name': obj.zconf.long_name,
						'category': obj.zconf.category,
						'author': obj.zconf.author,
						'version': obj.zconf.version,
						'description': obj.zconf.description
					},
					'attrib': {
						'zone_name': obj.zconf.name,
						'gates': obj.zconf.gates,
						'buffer': obj.zconf.buffer,
						'args': obj.zconf.args
					}
				}

			logger.debug('Zone attributes: ' + str(zone_attribs[zone_name]))

		return zone_attribs
