import sys, os

''' ZONE CONF CONSTS '''
ZNAME			= 1					# Zone name
GATES			= 2					# Zone gate allocation
ZOBJ			= 3					# Zone object (used internally)
ZARGS			= 100				# Zone specific arguments

''' LOG LEVEL CONSTS '''
DEBUG			= 10
INFO			= 20
WARN			= 30
ERROR			= 40
FATAL			= 50

LOG_FMT_CON		= '%(name)s - %(levelname)s - %(message)s'
LOG_FMT_FILE	= '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

''' FLASK CONF '''
API_URI_ROOT	= '/api/v0.1/'

''' ASSIGN DIRS '''
APP_ROOT		= (os.path.abspath(os.path.join(os.path.dirname(__file__),"..")) + '/')

PLUGIN_DIR		= APP_ROOT + 'enabled_zones/'
LOG_DIR			= APP_ROOT + 'logs/'
CONF_DIR		= 'conf/'

''' CONF FILE LOCATIONS '''
ZONE_CONF		= APP_ROOT + CONF_DIR + 'zones.json'

''' WGUI FILE LOCATION '''
WWW_DIR			= 'www/'
STATIC_DIR		= APP_ROOT + WWW_DIR + 'static/'
TEMPLATE_DIR	= APP_ROOT + WWW_DIR + 'templates/'

''' NETWORK SETUP '''
''' Left side, untrusted network '''
UNTRUST_IFACE = 'eth0'
UNTRUST_BR = 'UNTR_BR'				# Zones should use this to sniff

UNTRUST_NW = '192.168.56.0'			# Network for the Untrusted network
UNTRUST_NMASK = '/24'				# Untrusted network netmask
UNTRUST_GW_IP = '192.168.56.254'	# Untrusted gateway IP (set mitm'd clients to use this gateway), last octet should be 2 digits higher than the CLIENT_LIMIT as IP's for the exit zone are subtracted from this
UNTRUST_BR_IP = '192.168.56.200'	# This only needs to be set to provide the ARP function
CLIENT_LIMIT = 50					# Limit access to the first x clients - limited to 253
CLIENT_OVERWRITE = True				# NOT IN USE! Prefer to serve new clients over old ones (FIFO)

''' Right side, trusted network '''
TRUST_IFACE = 'eth1'
TRUST_BR = 'TR_BR'
EXIT_BR = 'EXIT_BR'					# Exit zones should attach an interface to this bridge

TRUST_NW = '10.0.3.0'				# Network for the Trusted network
TRUST_NAT_IP = '10.0.3.15'			# IP for NAT interface on the Trusted network
TRUST_GW_IP = '10.0.3.2'			# Real gateway IP
TRUST_NMASK	= '/24'					# Trusted network netmask

''' Management interface '''
MAN_IFACE1 = 'MAN_1'
MAN_IFACE2 = 'MAN_2'
MAN_IP = '10.0.3.55'				# Management GUI is accessible here

''' Override any options here with user specified ones in the app_conf file '''
from conf.app_conf import *

''' NET_SETUP
First part of tuple on startup, second part is run on shutdown. 'None' can be used to infer no action required
'''
NET_SETUP = []
NET_SETUP.append(( ('service network-manager stop'),('service network-manager start') ))
''' Remove IP's '''
NET_SETUP.append(( ('ip addr flush dev %s' % (UNTRUST_IFACE)), (None) ))
NET_SETUP.append(( ('ip addr flush dev %s' % (TRUST_IFACE)), (None) ))
''' Setup the untrusted bridge '''
NET_SETUP.append(( ('brctl addbr %s' % (UNTRUST_BR)), ('brctl delbr %s' % (UNTRUST_BR)) ))
NET_SETUP.append(( ('ip link set %s up' % (UNTRUST_BR)), ('ip link set %s down' % (UNTRUST_BR)) ))
NET_SETUP.append(( ('brctl addif %s %s' % (UNTRUST_BR, UNTRUST_IFACE)), (None) ))
NET_SETUP.append(( ('iptables -A INPUT -p ip -i %s -j DROP' % (UNTRUST_BR)), ('iptables -D INPUT -p ip -i %s -j DROP' % (UNTRUST_BR)) ))
NET_SETUP.append(( ('sysctl -w net.ipv4.conf.UNTR_BR.arp_notify=1'), ('sysctl -w net.ipv4.conf.UNTR_BR.arp_notify=0') ))
''' Setup the trusted bridge '''
NET_SETUP.append(( ('brctl addbr %s' % (TRUST_BR)), ('brctl delbr %s' % (TRUST_BR)) ))
NET_SETUP.append(( ('ip link set %s up' % (TRUST_BR)), ('ip link set %s down' % (TRUST_BR)) ))
NET_SETUP.append(( ('brctl addif %s %s' % (TRUST_BR, TRUST_IFACE)), (None) ))
NET_SETUP.append(( ('ip addr add %s%s dev %s' % (TRUST_NAT_IP, TRUST_NMASK, TRUST_BR)), (None) ))
NET_SETUP.append(( ('ip route add default via %s' % (TRUST_GW_IP)), ('ip route del default via %s' % (TRUST_GW_IP)) ))
''' Setup management interfaces '''
NET_SETUP.append(( ('ip link add %s type veth peer name %s' % (MAN_IFACE1, MAN_IFACE2)), ('ip link del %s' % (MAN_IFACE1)) ))
NET_SETUP.append(( ('brctl addif %s %s' % (TRUST_BR, MAN_IFACE1)), (None) ))
NET_SETUP.append(( ('ip link set %s up' % (MAN_IFACE1)), ('ip link set %s down' % (MAN_IFACE1)) ))
NET_SETUP.append(( ('ip link set %s up' % (MAN_IFACE2)), ('ip link set %s down' % (MAN_IFACE2)) ))
NET_SETUP.append(( ('ip addr add %s dev %s' % (MAN_IP, MAN_IFACE2)), (None) ))
''' Setup exit methods for zones '''
NET_SETUP.append(( ('brctl addbr %s' % (EXIT_BR)), ('brctl delbr %s' % (EXIT_BR)) ))
NET_SETUP.append(( ('ip link set %s up' % (EXIT_BR)), ('ip link set %s down' % (EXIT_BR)) ))
NET_SETUP.append(( ('ip addr add %s%s dev %s' % (UNTRUST_GW_IP, UNTRUST_NMASK, EXIT_BR)), (None) ))
NET_SETUP.append(( ('iptables -t nat -A POSTROUTING -o ' + TRUST_BR + ' -j MASQUERADE'),('iptables -t nat -D POSTROUTING -o ' + TRUST_BR + ' -j MASQUERADE') ))
NET_SETUP.append(( ('iptables -A FORWARD -i %s -j ACCEPT' % (EXIT_BR)), ('iptables -D FORWARD -i %s -j ACCEPT' % (EXIT_BR)) ))
NET_SETUP.append(( ('sysctl -w net.ipv4.ip_forward=1'), ('sysctl -w net.ipv4.ip_forward=0') ))
NET_SETUP.append(( ('sysctl -w net.ipv6.conf.all.disable_ipv6=1'), ('sysctl -w net.ipv6.conf.all.disable_ipv6=0') ))
