from flask import Flask, request, jsonify, render_template
import logging, logging.handlers, json

from lib import zone_manager, plugin_manager, json_parser
from lib.const import API_URI_ROOT, ZONE_CONF, STATIC_DIR, TEMPLATE_DIR, PLUGIN_DIR

class manage:

	def __init__(self):
		logger = logging.getLogger('pktjumble.flask_manager')

		logger.debug('Initalising web gui')

		self.zone_man = None
		self.flask = Flask('flask_manager', static_url_path='/static', static_folder=STATIC_DIR, template_folder=TEMPLATE_DIR)

		socketHandler = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
		
		werkzeug_logger = logging.getLogger('werkzeug')
		werkzeug_logger.addHandler(socketHandler)

		self.zone_conf_dict = None
		self.zone_man_loaded = False

		''' Site construction '''
		@self.flask.route(API_URI_ROOT)
		def build_page():
			logger = logging.getLogger('pktjumble.flask_manager.build_page')

			logger.debug('Returning core page')

			return render_template('main.html'), 200

		'''
		@self.flask.route(API_URI_ROOT + 'app/get_diagram/<zn>')
		def get_diagram(zn):
			logger = logging.getLogger('pktjumble.flask_manager.get_diagram')

			
			# if exist (PLUGIN_DIR/zn/icon.png):
				# icon = base64(PLUGIN_DIR/zn/icon.png)
			# else:
				icon = base64(STATIC_DIR/icon.png)

			logger.debug('Requested diagram for zone: ' + zn)
			
			return 'Nothing yet!' + zn
		'''

		''' Zone manager control '''
		@self.flask.route(API_URI_ROOT + 'zm/init')
		def init_zone_manager():
			logger = logging.getLogger('pktjumble.flask_manager.init_zone_manager')

			logger.debug('Initiating new zone manager')
			self.zone_man = zone_manager.manage()
			
			if len(self.zone_man.available_zones) == 0:
				logger.debug('Zone manager could not find any zones to load')
				self.zone_man_loaded = False
				resp = jsonify({'response': 'false', 'error_msg': 'Error loading zone manager, could not find any zones to load.'})
				resp.status_code = 500
			else:
				logger.debug('Zone manager initalised')
				self.zone_man_loaded = True
				resp = jsonify({'response': 'true'})
				resp.status_code = 200

			return resp

		@self.flask.route(API_URI_ROOT + 'zm/get_available_zones')
		def get_available_zones():
			logger = logging.getLogger('pktjumble.flask_manager.get_available_zones')

			if self.zone_man_loaded:
				logger.debug('Getting available zones and their attributes')
				attribs = self.zone_man.get_available_zones()
				resp = jsonify({'available_zones': attribs})
				resp.status_code = 200
			else:
				logger.debug('Zone manager currently not loaded')
				resp = jsonify({'response': 'false', 'error_msg': 'Zone manager not loaded yet.'})
				resp.status_code = 500

			return resp

		@self.flask.route(API_URI_ROOT + 'zm/start', methods=['GET','POST'])
		def start_zone_manager():
			logger = logging.getLogger('pktjumble.flask_manager.start_zone_manager')

			if self.zone_man_loaded:
				if request.headers['Content-Type'] == 'application/json':
					zone_conf_json = json.dumps(request.json)

					logger.debug('Converting zone json conf to dict conf')
					# Format the zone config correctly
					self.zone_conf_dict = json_parser.json2dict(zone_conf_json)

					if not self.zone_conf_dict:
						logger.info('Error parsing zone config')
						resp = jsonify({'response': 'false', 'error_msg': 'JSON provided is invalid.'})
						resp.status_code = 500

					logger.debug('Starting zones')
					self.zone_man.start(self.zone_conf_dict)
					resp = jsonify({'response': 'true'})
					resp.status_code = 200

				else:
					resp = jsonify({'response': 'false', 'error_msg': 'Zone config required in POST.'})
					resp.status_code = 500
			else:
				logger.debug('Zone manager currently not loaded')
				resp = jsonify({'response': 'false', 'error_msg': 'Zone manager not loaded yet.'})
				resp.status_code = 500

			return resp

		@self.flask.route(API_URI_ROOT + 'zm/stop')
		def stop_zone_manager():
			logger = logging.getLogger('pktjumble.flask_manager.stop_zone_manager')

			if self.zone_man_loaded:
				logger.debug('Stopping zones')
				self.zone_man.stop()
				self.zone_man_loaded = False
				resp = jsonify({'response': 'true'})
				resp.status_code = 200
			else:
				logger.debug('Zone manager currently not loaded')
				resp = jsonify({'response': 'false', 'error_msg': 'Zone manager not loaded yet.'})
				resp.status_code = 500

			return resp

	def start(self):
		logger = logging.getLogger('pktjumble.flask_manager.start')

		logger.debug('Starting flask')
		self.flask.run(debug=True, use_reloader=False, port=5000)






'''

# How to stop flask manually

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'
    
    
    
# Run my code after flask dies

if __name__ == '__main__':
    init_db()  #or what you need
    try:
        app.run(host="0.0.0.0")
    finally:
        # your "destruction" code
        print 'Can you hear me?'

    
'''
