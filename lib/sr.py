import logging, logging.handlers

import socket, commands, re, struct, signal, binascii
from multiprocessing import Process, Queue, Manager

class sr:

	def __init__(self, iface, rcv_q=None, snd_q=None, staticmac=None):
		logger = logging.getLogger('pktjumble.sr.init')

		self.arp_table = Manager().dict()

		self.rcv_q = Queue() if rcv_q == None else rcv_q
		self.snd_q = Queue() if snd_q == None else snd_q

		self.iface = iface

		''' Obtain our mac address & add to mac table '''
		HWaddr = commands.getoutput('ifconfig ' + iface + ' | grep HWaddr')
		regmac = re.search('([0-9A-F]{2}[:-]){5}([0-9A-F]{2})', HWaddr, re.I)

		try:
			xDstMac = re.sub('[:-]', '', regmac.group())
		except AttributeError, e:
			logger.error('Interface %s does not exist' % iface)
			raise Exception('Interface %s does not exist' % iface)

		self.arp_table['self'] = binascii.unhexlify(xDstMac)
		logger.debug('Added my own MAC/IP pair to _sr_ ARP table: %s' % (xDstMac))

		if staticmac:
			for ip, mac in staticmac.iteritems():
				xDstIP = binascii.hexlify(socket.inet_aton(ip)).decode('hex')
				xDstMac = binascii.unhexlify(mac.replace(':', ''))
				self.arp_table[xDstIP] = xDstMac

				logger.debug('Static MAC/IP assigned to the _sr_ ARP table: MAC[%s], IP[%s]' % (xDstMac.encode('hex'), xDstIP.encode('hex')))

		self.threads = []

	def start(self):
		logger = logging.getLogger('pktjumble.sr.start')

		rcv_th = Process(target=self._rcv, args=(self.iface, self.rcv_q, self.arp_table,))
		rcv_th.start()

		snd_th = Process(target=self._snd, args=(self.iface, self.snd_q, self.arp_table,))
		snd_th.start()

		self.threads.append(rcv_th)
		self.threads.append(snd_th)

		logger.debug('Started snd/rcv threads')

	def stop(self):
		logger = logging.getLogger('pktjumble.sr.stop')

		for th in self.threads:
			th.terminate()

		logger.debug('Stopped snd/rcv threads')

	def _rcv(self, iface, q, arp_table):
		logger = logging.getLogger('pktjumble.sr.rcv')
		signal.signal(signal.SIGINT, self.gotkill)

		s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x800))
		s.bind((iface, 0x800))
		logger.debug('Rcv bound to %s' % iface)

		c = 0

		while True:

			try:
				xP, sa_ll = s.recvfrom(65565)
			except IOError, e:
				break

			''' Test if packet directed to the host '''
			if sa_ll[2] in [socket.PACKET_OUTGOING, socket.PACKET_BROADCAST, socket.PACKET_MULTICAST]:
				continue
			''' Test is packet is IP '''
			if not sa_ll[1] == 0x800:
				continue
			''' Test for IPv4 '''
			if not struct.unpack('>b', xP[14])[0] >> 4  == 4:
				continue

			logger.debug('Sniffed packet #%s: %s' % (c, xP.encode('hex')))

			''' Add source IP to ARP table '''
			xSrcMac	= xP[6:12]
			xSrcIP	= xP[30:34]

			if not xSrcIP in arp_table:
				arp_table[xSrcIP] = xSrcMac
				logger.debug('Added: IP[%s] MAC[%s]' % (xSrcIP.encode('hex'), xSrcMac.encode('hex')))

			''' Strip to IP layer '''
			xIP = xP[14:]

			q.put(xIP)
			logger.debug('Put packet #%s on queue' % str(c))

			c += 1

	def _snd(self, iface, q, arp_table):
		logger = logging.getLogger('pktjumble.sr.snd')
		signal.signal(signal.SIGINT, self.gotkill)

		s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
		s.bind((iface, 0x800))
		logger.debug('Snd bound to %s' % iface)

		c = 0

		while True:

			try:
				xIP = q.get()
			except IOError, e:
				break

			logger.debug('Got packet #%s from queue' % str(c))

			xDstIP = xIP[12:16]

			try:
				logger.debug('Looking up IP[%s]' % (xDstIP.encode('hex')))
				xDstMac = arp_table[xDstIP]
			except KeyError, e:
				logger.debug('No entry in arp_table for packet #%s, dropping' % str(c))
				continue

			''' Construct Ether layer '''
			xP = xDstMac
			xP += arp_table['self']
			xP += '\x08\x00'

			''' Add current IP layer '''
			xP += xIP

			s.send(xP)

			logger.debug('Sent packet #%s: %s ', c, xP.encode('hex'))
			c += 1

	''' For BPF filter - Not my code! http://fossies.org/linux/scapy/scapy/arch/linux.py
	def set_filter(s, filter):
		logger = logging.getLogger('pktjumble.sr.set_filter')

		if not TCPDUMP:
			return
		try:
			f = os.popen("%s -i %s -ddd -s 1600 '%s'" % (conf.prog.tcpdump,conf.iface,filter))
		except OSError,msg:
			log_interactive.warning("Failed to execute tcpdump: (%s)")
			return
		lines = f.readlines()
		if f.close():
			raise Scapy_Exception("Filter parse error")
		nb = int(lines[0])
		bpf = ""
		for l in lines[1:]:
			bpf += struct.pack("HBBI",*map(long,l.split()))

		# XXX. Argl! We need to give the kernel a pointer on the BPF,
		# python object header seems to be 20 bytes. 36 bytes for x86 64bits arch.
		if scapy.arch.X86_64:
			bpfh = struct.pack("HL", nb, id(bpf)+36)
		else:
			bpfh = struct.pack("HI", nb, id(bpf)+20)  
		s.setsockopt(SOL_SOCKET, SO_ATTACH_FILTER, bpfh)
	'''

	def gotkill(self, a,b):
		pass

if __name__ == '__main__':

	sr = sr('eth0')

	sr.start()

	try:
		while True:

			xIP = sr.rcv_q.get()
			bIP = bytearray(xIP)

			bIP[20] = '\x00'

			srcip = bIP[12:16]
			dstip = bIP[16:20]

			bIP[12:16] = dstip
			bIP[16:20] = srcip

			sr.snd_q.put(str(bIP))

	except KeyboardInterrupt, e:
		sr.stop()
