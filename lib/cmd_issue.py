import logging, logging.handlers, subprocess

def setup(CMD_LIST):
	logger = logging.getLogger('pktjumble.cmdrun.setup')

	logger.info('Running first commands')
	''' Running first commands '''
	for i in range(len(CMD_LIST)):

		try:

			logger.debug('Running command: ' + CMD_LIST[i][0])
			subprocess.check_call(CMD_LIST[i][0], shell=True)

		except subprocess.CalledProcessError, error:
			logger.fatal('Error running command, reversing. Errored at command: %s' % (str(error.cmd)) )
			
			for x in reversed(range(i+1)):

				if CMD_LIST[x][1] != None:
					logger.debug('Reversing command: ' + CMD_LIST[x][1])
					subprocess.check_call(CMD_LIST[x][1], shell=True)

			raise Exception('Could not complete command: %s' % (str(error.cmd)) )

def teardown(CMD_LIST, force=False):
	logger = logging.getLogger('pktjumble.cmdrun.teardown')

	if force:
		logger.info('FORCEFULLY tearing down commands')
	else:
		logger.info('Tearing down commands')

	''' Running second commands '''
	for i in reversed(range(len(CMD_LIST))):

		try:

			if CMD_LIST[i][1] != None:
				logger.debug('Running command: ' + CMD_LIST[i][1])
				subprocess.check_call(CMD_LIST[i][1], shell=True)

		except subprocess.CalledProcessError, error:
			if force:
				logger.error('Error running command, but we continue. Errored at command: ' + str(error.cmd))
			else:
				raise Exception('Error tearing down commands. Errored at command: ' + str(error.cmd))
