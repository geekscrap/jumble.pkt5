import logging, logging.handlers, os, imp, sys
from lib.const import PLUGIN_DIR

def load_zones():
	logger = logging.getLogger('pktjumble.plugin_manager')

	main_module = 'zone'
	available_zones = {}
	
	possibleplugins = os.listdir(PLUGIN_DIR)
	for zone_dir in possibleplugins:
		location = os.path.join(PLUGIN_DIR, zone_dir)
		if not os.path.isdir(location) or not main_module + ".py" in os.listdir(location):
			continue
		else:
			try:
				info = imp.find_module(main_module, [location])
				logger.info('Found loadable zone: ' + str(zone_dir))

				imp.load_module('zone', *info)
				logger.info('Imported zone: ' + str(zone_dir))
			except Exception, error:
				logger.error('Error importing the zone \'' + str(zone_dir) + '\'. Error: ' + str(error))

			loaded_zone = sys.modules.pop(main_module)
			sys.modules[('zone_' + str(loaded_zone.zconf.name))] = loaded_zone

			available_zones[str(loaded_zone.zconf.name)] = loaded_zone

	return available_zones
