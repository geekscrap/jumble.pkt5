# USER CONFIG

''' Logging format '''
LOG_FMT_CON		= '%(name)s - %(levelname)s - %(message)s'
LOG_FMT_FILE	= '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

''' Web GUI options '''
API_URI_ROOT	= '/api/v0.1/'
WWW_DIR			= '/var/www/jumble-pkt5/'
STATIC_DIR		= '/var/www/jumble-pkt5/static/'
TEMPLATE_DIR	= '/var/www/jumble-pkt5/templates/'

''' Directory assignments '''
PLUGIN_DIR		= './enabled_zones/'
LOG_DIR			= './logs/'
CONF_DIR		= './conf/'

''' Network setup '''
# DEFINE!